ARG ALPINE_VERSION=3.16

FROM alpine:${ALPINE_VERSION} as builder

ARG ZATHURA_VERSION=0.4.9
ARG ZATHURA_MUPDF_VERSION=0.3.8

RUN apk add --no-cache \
        appstream-glib \
        build-base \
        check-dev \
        curl \
        desktop-file-utils \
        file-dev \
        girara-dev \
        gumbo-parser-dev \
        intltool \
        jbig2dec-dev \
        libjpeg-turbo-dev \
        libseccomp-dev \
        meson \
        mupdf \
        mupdf-dev \
        ncurses \
        ninja \
        openssl1.1-compat-dev \
        openjpeg-dev \
        patch \
        py3-docutils \
        py3-sphinx \
        sqlite-dev \
        tar \
        texlive-dev \
        xz && \
    curl -fLo zathura-${ZATHURA_VERSION}.tar.gz \
        https://git.pwmt.org/pwmt/zathura/-/archive/${ZATHURA_VERSION}/zathura-${ZATHURA_VERSION}.tar.gz && \
    curl -fLo zathura-pdf-mupdf-${ZATHURA_MUPDF_VERSION}.tar.xz \
        https://pwmt.org/projects/zathura-pdf-mupdf/download/zathura-pdf-mupdf-${ZATHURA_MUPDF_VERSION}.tar.xz && \
    curl -fLo fix-meson.build.patch \
        https://git.alpinelinux.org/aports/plain/community/zathura-pdf-mupdf/fix-meson.build.patch?h=3.16-stable && \
    tar xzf zathura-${ZATHURA_VERSION}.tar.gz && \
    cd zathura-${ZATHURA_VERSION} && \
    meson build && \
    meson compile -C build && \
    DESTDIR=/usr/local/zathura meson install -C build && cd - && \
    tar xJf zathura-pdf-mupdf-${ZATHURA_MUPDF_VERSION}.tar.xz && \
    cd zathura-pdf-mupdf-${ZATHURA_MUPDF_VERSION} && \
    patch -p1 < ../fix-meson.build.patch && \
    export PKG_CONFIG_PATH=/usr/local/zathura/usr/local/lib/pkgconfig && \
    meson build -Dc_args=-I/usr/local/zathura/usr/local/include \
        -Dc_link_args=-L/usr/local/zathura/usr/local/lib && \
    meson compile -C build && \
    DESTDIR=/usr/local/zathura meson install -C build


FROM alpine:${ALPINE_VERSION}

COPY --from=builder /usr/local/zathura/usr/local/bin/zathura /usr/local/bin/
COPY --from=builder /usr/local/zathura/usr/local/lib/zathura /usr/local/lib/zathura
RUN apk add --no-cache \
        cairo \
        girara \
        glib \
        gtk+3.0 \
        libintl \
        libmagic \
        libseccomp \
        libsynctex \
        mupdf \
        pango \
        sqlite-libs

CMD ["zathura"]
